@extends('layouts.main')

@section('content')
<nav id="main-navigation" class="mx-auto text-center">
    @auth
    <p>{{auth()->user()->name}} | {{auth()->user()->username}}</p>
    <a href="/admin">Dashboard</a>
    <a href="/{{auth()->user()->username}}">Carta</a>
    <form method="POST" action="/logout">
        @csrf
        <button type="submit">Logout</button>
    </form>
    @else
        <a href="/login">Login</a>
        <a href="/register">Registrarse</a>
    @endauth
</nav>
@endsection
