@extends('layouts.main')

@section('content')
    <div class="form-container">
        <div>
            <div>
                <div class="form-card">

                    <h4>
                        {{ __('Login') }}
                    </h4>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div>
                            <label for="name">
                                @include('icon.email')
                                <input placeholder="Su email" id="email" type="text" name="email" required autocomplete="email">
                            </label>
                        </div>

                        <div>
                            <label for="name">
                                @include('icon.password')
                                <input placeholder="Contraseña" id="password" type="password" name="password" required autocomplete="new-password">
                            </label>
                        </div>

                        <div>
                            <label class="remember-me" for="remember">
                                <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                <span>{{ __('Recuérdame') }}</span>
                            </label>
                        </div>

                        <div>
                            <button type="submit">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                                <a class="forgot-password" href="{{ route('password.request') }}">
                                    {{ __('Olvidaste tu contraseña?') }}
                                </a>
                            @endif

                            @if (Route::has('register'))
                                <p>
                                    {{ __("No tienes una cuenta?") }}
                                    <a href="{{ route('register') }}">
                                        {{ __('Registrate') }}
                                    </a>
                                </p>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
