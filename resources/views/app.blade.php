@extends('layouts.main')

@section('head')
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
rel="stylesheet">
<script> const data = @json($data); </script>
@endsection

@section('content')

{{-- root Vue component --}}
<div id="app">
    <div id="m-navbar">
        <img src="/storage/{{auth()->user()->logo}}" alt="">
        <div>
            <router-link to="/app/">Admin</router-link>
            <router-link to="/app/carta">Carta</router-link>
            <router-link to="/app/perfil">Perfil</router-link>
            <router-link to="/app/qr">QR</router-link>
            <form method="POST" action="/logout">
                @csrf
                <button type="submit">Logout</button>
            </form>
        </div>
    </div>
    <div>
        <router-view></router-view>
    </div>
</div>{{-- end Vue root component --}}
<div>
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 18c1.657 0 3 1.343 3 3s-1.343 3-3 3-3-1.343-3-3 1.343-3 3-3zm0-9c1.657 0 3 1.343 3 3s-1.343 3-3 3-3-1.343-3-3 1.343-3 3-3zm0-9c1.657 0 3 1.343 3 3s-1.343 3-3 3-3-1.343-3-3 1.343-3 3-3z"/></svg>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection

