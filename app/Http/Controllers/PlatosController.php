<?php

namespace App\Http\Controllers;

use App\Plato;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;


class PlatoUtils
{

		public static function validation(Request $request)
		{
			return Validator::make($request->all(), [
				'title' => 'required|string|max:80',
				'price' => 'required',
				'categoria_id' => 'required|integer',
				'alergenos' => 'sometimes|string',
				'description' => 'nullable|string|max:512',
				'picture' => 'sometimes|image|mimes:jpg,png,jpeg',
		]);
		}

    /**
		 * Crea un plato nuevo en base al modelo
		 * solo se llama despues de validar la data del request
     * @param Request $request
		 * @param Plato $plato
     * @return Plato instance
     */
		public static function create(Request $request, Plato $plato)
		{
			// Primero guardamos la data obligatoria
			$plato->user_id = auth()->user()->id;
			$plato->title = ucfirst($request['title']);
			$plato->price = floatval($request['price']);
			$plato->categoria_id = $request['categoria_id'];
			// Ahora preguntamos por la data opcional
			$plato->description = !empty($request['description']) ? $request['description'] : null;
			return $plato;
		}

    /**
		 * Busca un plato por nombre y usuario
		 * @param string $title
     *
     */
		public static function findNameInThisUser(string $title)
		{
			$plato = Plato::where('title', $title)->where('user_id', auth()->user()->id)->first();
			return  $plato ? $plato : false;
		}

		/**
		 * Busca un plato por id y usuario
		 * @param string $title
     *
     */
		public static function findPlatoByIdInUser(int $id)
		{
			$plato = Plato::where('id', $id)->where('user_id', auth()->user()->id)->first();
			return  $plato ? $plato : false;
		}
}

class PlatosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index(/* void */)
    {
			$user_id = auth()->user()->id;
			$user = User::find($user_id);
			if ($user)
			{
				return $user->platos;
			}
			else
			{
				abort(403);
			}
    }

    /**
     * Store a newly created resource in storage.
     * FORMAT:
		 * {
		 * 		"title": @string,
		 * 		"price": @int,
		 * 		"categoria_id": @int
		 * 		"description": @string
		 * 		"picture": @string
		 * }
     * @param  \Illuminate\Http\Request $request
     * @return
     */
    public function store(Request $request)
    {
        $validator = PlatoUtils::validation($request);
        error_log($validator->errors());
        // SI FALLA LA VALIDACION MENSAJE DE ERROR
        if ($validator->fails()){
            return response()->json([
                "msg" => "El formato enviado no es correcto",
                "body" => $validator->errors(),
                "old" => $request->all()
						], 400);
				}
				// CHECK IF PLATO NAME EXIST
				if (PlatoUtils::findNameInThisUser($request['title']))
				{
					return response()->json([
							"msg" => sprintf('Ya existe un plato con el nombre "%s".', $request['title']),
							"body" => null,
							"old" => $request->all()
					], 400);
				}
        // SI LA VALIDACION ES EXITOSA:
        $plato = PlatoUtils::create($request, new Plato());
        $plato->picture = !empty($request['picture']) ? '/storage/' . $request['picture']->store('picture') : null;
        $plato->save();
        $plato->alergenos()->attach(explode(',', $request['alergenos']));
        return response()->json([
            "msg" => "plato creado correctamente",
						"body" => $plato,
						"old" => null
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
			PlatoUtils::findPlatoByIdInUser($request['id']);
		}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
			$validator = PlatoUtils::validation($request);
			// SI FALLA LA VALIDACION MENSAJE DE ERROR
			if ($validator->fails()){
					return response()->json([
							"msg" => "El formato enviado no es correcto",
							"body" => $validator->errors(),
							"old" => $request->all()
					], 400);
			}
			// CHECK IF PLATO NAME EXIST
			$platoInstance = PlatoUtils::findPlatoByIdInUser($request['plato_id']);
			if (!$platoInstance)
			{
				return response()->json([
						"msg" => "no existe ese plato.",
						"body" => $platoInstance,
						"old" => $request->all()
				], 400);
			}
            // SI LA VALIDACION ES EXITOSA:
            // return $request;
            $plato = PlatoUtils::create($request, $platoInstance);
            if (!empty($request['categoria_id']))
            {
                $plato->categoria_id = $request['categoria_id'];
            }
            if (!empty($request['picture']))
            {
                $plato->picture = $request['picture']->store('picture');
            }
            $plato->update();
            if (!empty($request['alergenos']))
            {
                $plato->alergenos()->sync(explode(',', $request['alergenos']));
            }
			return response()->json([
					"msg" => "plato editado correctamente",
					"body" => $plato,
					"old" => $platoInstance
			]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
			$validator = PlatoUtils::validation($request);
			// SI FALLA LA VALIDACION MENSAJE DE ERROR
			if ($validator->fails()){
					return response()->json([
							"msg" => "El formato enviado no es correcto",
							"body" => $validator->errors(),
							"old" => $request->all()
					], 400);
			}
			// CHECK IF PLATO NAME EXIST
			$platoInstance = PlatoUtils::findNameInThisUser($request['title']);
			if (!$platoInstance)
			{
				return response()->json([
						"msg" => sprintf('No existe un plato con el nombre "%s".', $request['title']),
						"body" => $platoInstance,
						"old" => $request->all()
				], 400);
			}
			// SI LA VALIDACION ES EXITOSA:
			$oldTitle = $platoInstance->title;
			if ($platoInstance->picture)
			{
				Storage::delete($platoInstance->picture);
			}
			$platoInstance->delete();
			return response()->json([
					"msg" => sprintf('El plato "%s" fue eliminado de forma exitosa.', $oldTitle),
					"body" => $platoInstance,
					"old" => null
			]);
    }


    public function like(Request $request)
    {
        // entonctrar un plato con la id del request
        $plato = Plato::where('id', $request['id'])->first();
        // llamar al metodo like de ese palto
        error_log($plato->likes);
        $plato->likes = $plato->likes + 1;
        $plato->update();
        $res = $plato->likes;
        return response()->json([
            "totalLikes" => $res
        ]);
    }
}
