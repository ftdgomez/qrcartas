<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProfileUtils
{
    public static function validation(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:42'],
            'username'=>['required', 'string', 'max:42','alpha_dash', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['sometimes', 'string', 'numeric', 'digits:9'],
            'logo' => ['sometimes','image', 'mimes:jpg,png,jpeg'],
            'direction' => ['sometimes', 'string']
        ]);
    }
}

class ProfileController extends Controller
{
    public function edit()
    {
        return view('auth.profile');
    }

    public function update(Request $request)
    {
        $validator = ProfileUtils::validation($request);
        if ($validator->fails())
        {
            return response()->json([
                "msg" => "El formato enviado no es correcto",
                "body" => $validator->errors(),
                "old" => $request->all()
						], 400);
		}
        $user = auth()->user();
        $user->username = $request['username'];
        $user->name = $request['name'];
        if($request['direction'])
        {
            $user->direction;
        }
        if ($request['logo'])
        {
            if ($user->logo){
                Storage::delete($user->logo);
            }
            $user->logo = $request['logo']->store('logos');
        }
        $user->update();
        return redirect('/admin');
    }
}
