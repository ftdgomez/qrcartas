import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './routes';
import axios from 'axios';
window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
import VueFlashMessage from 'vue-flash-message';

Vue.use(VueRouter)
Vue.use(VueFlashMessage);

let app = new Vue({
    el: '#app',

    router: new VueRouter(routes)
});
