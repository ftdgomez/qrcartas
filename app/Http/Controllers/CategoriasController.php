<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class CategoriaUtils
{
		/**
		 * Encuentra una categoria por su id
		 *
		 * @return Categoria or false
		 */
		public static function exist(int $cat_id)
		{
			$cat = Categoria::where('id', $cat_id)->first();
			return  $cat ? $cat : false;
		}
		/**
		 * Encuentra una categoria por su nombre
		 *
		 * @return true if exist, false if not.
		 */
		public static function findByName(string $name)
		{
			return Categoria::where('name', $name)->first() ? true : false;
		}
		/**
		 * Valida la data entrante
		 */
		public static function validation(Request $request)
		{
			return Validator::make($request->all(), [
				'name' => 'required|string|max:80',
				'cat_id' => 'sometimes|integer'
			]);
		}
		public static function findNameInThisUser(string $name)
		{
			return Categoria::where('name', $name)->where('user_id', auth()->user()->id)->first() ? true : false;
		}
}

class CategoriasController extends Controller
{

		/**
		 * Retorna todas las categorias de un usuario
		 * @method GET
		 * @return array categorias
		 */
		public function index(/* void */)
		{
			$user_id = auth()->user()->id;
			$user = User::find($user_id);
			if ($user)
			{
				return $user->categorias;
			}
			else
			{
				abort(403);
			}
		}

		/**
		 * Crea una categoria nueva
		 * @method POST
		 * FORMAT:
		 * {
		 * 		"name": @string
		 * }
		 * @return $request
		 */
    public function store(Request $request)
		{
			// Valida la data
			$validator = CategoriaUtils::validation($request);
			// Si la validación falla, retorna un mensaje de error
			if ($validator->fails()) {
				return response()->json([
						"msg" => "El formato enviado no es correcto",
						"body" => $validator->errors(),
						"old" => $request->all()
				], 400);
			}
			// Check que el usuario no repita la categoria
			if (CategoriaUtils::findNameInThisUser($request['name']))
			{
				// si la categoria está repitada, retorna error
				return response()->json([
						"msg" => sprintf('La categoria "%s" ya existe.', request('name')),
						"body" => null,
						"old" => $request->all()
				], 400);
			}
			// Si la validación es exitosa, entonces crea la categoria
			$categoria = new Categoria();
            $categoria->name = ucfirst(request('name'));
			$categoria->user_id = auth()->user()->id;
			$categoria->save();
			return response()->json([
				"msg" => sprintf('La categoria "%s" ha sido creada exitosamente', $categoria->name),
				"body" => $categoria,
				"old" => null
			], 200);
		}

		/**
		 * Editar Una Categoria existente
		 * @method POST
		 * FORMAT:
		 * {
		 * 		"cat_id": @integer,
		 * 		"name": @string
		 * }
		 * @return $request
		 */
		public function update(Request $request)
		{
			$validator = CategoriaUtils::validation($request);
			// Si la validación falla, retorna un mensaje de error
			if ($validator->fails()) {
				return response()->json([
						"msg" => "El formato enviado no es correcto",
						"body" => $validator->errors(),
						"old" => $request->all()
				], 400);
			}
			// check if cat_id is in the request
			if (!request('cat_id'))
			{
				return response()->json([
					"msg" => "Debes especificar la categoria que quieres editar.",
					"body" => null,
					"old" => $request->all()
			], 400);
			}
			// Si la validación es exitosa, chequea que la categoria exista
			$categoria = CategoriaUtils::exist(request('cat_id'));
			if (!$categoria)
			{
				return response()->json([
					"msg" => sprintf('La categoria "%s" no existe asi que no puede ser editada', request('name')),
					"body" => null,
					"old" => $request->all()
			], 400);
			}
			// si todo va bien... Edita la categoria
			$old_name = $categoria->name;
			$categoria->name = request('name');
			$categoria->user_id = auth()->user()->id;
			$categoria->update();
			return response()->json([
				"msg" => sprintf('La categoria "%s" ha sido editada exitosamente y ahora se llama "%s"', $old_name, request('name')),
				"body" => $categoria,
				"old" => $request->all()
            ], 200);

		}

		/**
		 * ELIMINA Una Categoria existente
		 * @method POST
		 * FORMAT:
		 * {
		 * 		"cat_id": @integer,
		 * 		"name": @string
		 * }
		 * @return $request
		 */
		public function delete(Request $request)
		{
			$validator = CategoriaUtils::validation($request);
			// Si la validación falla, retorna un mensaje de error
			if ($validator->fails()) {
				return response()->json([
						"msg" => "El formato enviado no es correcto",
						"body" => $validator->errors(),
						"old" => $request->all()
				], 400);
			}
			// check if cat_id is in the request
			if (!request('cat_id'))
			{
				return response()->json([
					"msg" => "Debes especificar la categoria que quieres eliminar.",
					"body" => null,
					"old" => $request->all()
			], 400);
			}
			// Si la validación es exitosa, chequea que la categoria exista
			$categoria = CategoriaUtils::exist(request('cat_id'));
			if (!$categoria)
			{
				return response()->json([
					"msg" => sprintf('La categoria "%s" no existe asi que no puede ser eliminada', request('name')),
					"body" => null,
					"old" => $request->all()
			], 400);
			}
			// si todo va bien... ELIMINA la categoria
			$old_name = $categoria->name;
			$categoria->delete();
			$arr = $categoria->platos;
			foreach($arr as $c)
			{
				if ($c->picture)
				{
					Storage::delete($c->picture);
				}
				$c->delete();
			}
			return response()->json([
				"msg" => sprintf('La categoria "%s" ha sido ELIMINADA exitosamente', $old_name, request('name')),
				"body" => $categoria,
				"old" => $request->all()
			], 200);
		}
}
