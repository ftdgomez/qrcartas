<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plato extends Model
{
	  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
		protected $guarded = [];

		/**
		 * Retorna la categoria de un plato
		 *
		 * @return ref de la cateogoria a la que pertenece
		 */
    public function categoria()
		{
			return $this->belongsTo(Categoria::class);
		}

		/**
		 * Retorna el usuario creador de un plato
		 *
		 * @return ref a su usuario creador
		 */
		public function user()
		{
			return $this->belongsTo(User::class);
		}

		/**
		 * Retorna una lista de alergenos que contiene el plato
		 *
		 * @return arr de alergenos
		 */
		public function alergenos()
		{
			return $this->belongsToMany(Alergeno::class);
		}

		/**
		 * Dar like a un plato
		 *
		 * @return la cantidad de likes del plato
		 */
		public function like()
		{
			$this->likes = $this->likes + 1;
			$this->update();

			return $this->likes;
		}
}
