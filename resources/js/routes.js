import Admin from './components/Admin'
import Carta from './components/Carta'
import Perfil from './components/Perfil'
import CategoriaForm from './components/CategoriaForm'
import PlatoForm from './components/PlatoForm'
import Qr from './components/Qr';
import NotFound from './components/NotFound'

export default {
    mode: 'history',

    routes: [
        // define the 404 first with a *
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/app/',
            component: Admin
        },
        {
            path: '/app/admin',
            component: Admin
        },
        {
            path: '/app/carta',
            component: Carta
        },
        {
            path: '/app/perfil',
            component: Perfil
        },
        {
            path: '/app/categorias',
            component: CategoriaForm
        },
        {
            path: '/app/platos',
            component: PlatoForm
        },
        {
            path: '/app/qr',
            component: Qr
        }
    ]
}
