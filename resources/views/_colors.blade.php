<script src="{{ asset('js/vibrant.js') }}"></script>
<script>
    document.addEventListener("DOMContentLoaded", (event) => {
        let imgTag = document.querySelector('#imgPrev');
        let imgInput = document.querySelector('#logo');
        function readURL(input) {
            const reader = new FileReader();
            reader.onload = (e) => {
                imgTag.setAttribute('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
        imgInput.addEventListener('change', ()=>readURL(imgInput));
        imgTag.addEventListener('load', ()=>{
            let colorp;
            let colors;
            window.Vibrant.from(document.querySelector('#imgPrev')).getPalette(function(err, palette) {
                console.log(palette)
                colorp = palette.Muted.hex;
                colors = palette.DarkVibrant.hex;
                console.log(colorp, colors)
                if (colorp && colors)
                {
                    let hinput = document.querySelector('#theme_color');
                    hinput.value = `${colorp},${colors}`;
                }
            });
        })
    });
</script>
