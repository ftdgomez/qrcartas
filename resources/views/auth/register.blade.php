@extends('layouts.main')

@section('content')
<div class="form-container">
    <div class="form-card">
        <h4>
            {{ __('Formulario de registro') }}
        </h4>
        <form method="POST" action="{{route('register')}}" enctype="multipart/form-data">
            @csrf
            {{-- Inputs name & email --}}
            <div>
                <label for="name">
                    @include('icon.user')
                    <input placeholder="Su Nombre" id="name" type="text" name="name" required autocomplete="name" autofocus>
                </label>
                <label for="name">
                    @include('icon.email')
                    <input placeholder="Su email" id="email" type="text" name="email" required autocomplete="email">
                </label>
            </div>
            {{-- Input Username --}}
            <div>
                <div id="previewUsername">
                    <preview-username></preview-username>
                </div>
            </div>
            {{-- Password inputs --}}
            <div>
                <label for="name">
                    @include('icon.password')
                    <input placeholder="Contraseña" id="password" type="password" name="password" required autocomplete="new-password">
                </label>
                <label for="name">
                    @include('icon.password')
                    <input placeholder="Confirmar Contraseña" id="password-confirm" type="password" name="password_confirmation" required autocomplete="new-password">
                </label>
            </div>
            {{-- File upload --}}
            <div>
                <input type="file" name="logo" id="logo" />
                <label class="u-btn" for="logo"><span></span> <strong><svg style="fill: currentColor" xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"></path></svg>Subir logo</strong></label>
            </div>
            <p class="comment">
                Se recomienda utilizar una imagen en formato "png" transparente para mejores resultados.
            </p>
            <div>
                <div>
                    <button type="submit">
                        {{ __('Registrarse') }}
                    </button>
                </div>

                @if (Route::has('password.request'))
                <a class="forgot-password" href="{{ route('password.request') }}">
                    {{ __('Olvidaste tu contraseña?') }}
                    </a>
                @endif
                @if (Route::has('register'))
                    <p style="text-align: center">
                        {{ __("Ya tienes una cuenta?") }}
                        <a href="{{ route('login') }}">
                            {{ __('Iniciar sesión') }}
                        </a>
                    </p>
                @endif
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
    @include('_vue')
    <script>
        Vue.component('preview-username',{
            template: `
            <div>
                <div class="url-info" v-show="isFocus">
                    <p>Su url será:</p>
                    <p class="url" v-text="url + str"></p>
                    <p>Solo se permiten letras, números, guiones y barra baja</p>
                </div>
                <label for="username">
                    @include('icon.qr')
                    <input @click="()=>{isFocus=true;if(isFocus && isResetable){isResetable= false; str=''}}"
                    pattern="^[a-zA-Z0-9]*$" v-model="str" type="text" name="username" id="username" placeholder="Su usuario / url" maxlength="42" autocomplete="off" />
                </label>
            </div>
            `,
            data(){
                return {
                    str: '',
                    url: `${window.location.origin}/`,
                    isFocus: false,
                    isResetable: true
                }
            }
        })
        new Vue({
            el: '#previewUsername'
        })
    </script>
@endsection
