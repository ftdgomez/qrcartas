<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Qr') }}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/ftdgomez/makeup/src/fonts/gotham.css">
    <style>*{font-family: 'Gotham A', 'Gotham B';}body{padding: 0; margin: 0}</style>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    @yield('head')
</head>
<body class="">
    @yield('content')
    @yield('scripts')
</body>
</html>
