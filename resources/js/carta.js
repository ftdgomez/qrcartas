import Vue from 'vue';
import axios from 'axios';
window.axios = axios;
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';


import CartaList from './components/CartaList.vue'

new Vue({
    render: h => h(CartaList),
  }).$mount('#carta')
