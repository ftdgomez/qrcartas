<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alergeno extends Model
{
    public function platos()
		{
			return $this->belongsToMany(Plato::class);
		}
}
