<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlergenosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alergenos', function (Blueprint $table) {
						$table->id();
						$table->text('name');
            $table->timestamps();
				});
				Schema::create('alergeno_plato', function (Blueprint $table) {
					$table->primary(['alergeno_id', 'plato_id']);
					$table->unsignedBigInteger('plato_id');
					$table->unsignedBigInteger('alergeno_id');

					$table->foreign('plato_id')->references('id')->on('platos')->onDelete('cascade');
					$table->foreign('alergeno_id')->references('id')->on('alergenos')->onDelete('cascade');
					$table->timestamps();
			});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alergenos');
    }
}
