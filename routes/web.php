<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('home');
});
Route::middleware('auth')->group(function () {
    /* =========================================== */
    // Todo lo que va aqui dentro es privado
    // Categorias
    Route::get('/categorias', 'CategoriasController@index');
    Route::post('/categoria/store', 'CategoriasController@store');
    Route::post('/categoria/update', 'CategoriasController@update');
    Route::post('/categoria/delete', 'CategoriasController@delete');

    // Platos
    Route::get('/platos', 'PlatosController@index');
    Route::post('/plato/store', 'PlatosController@store');
    Route::post('/plato/update', 'PlatosController@update');
    Route::post('/plato/delete', 'PlatosController@destroy');

    // Profile
    Route::post('/updateprofile', 'ProfileController@update')->name('update');
    Route::get('/profile/edit', 'ProfileController@edit');

    Route::get('/admin', function () {
        return redirect('/app/');
    });
    // Todas las rutas que no están especificadas son manejadas por el router de Vue
    Route::get('/app/{any?}', 'CartaController@admin' );
});

Route::get('/{username}', 'CartaController@index');
/*
    Route::get('/home', 'HomeController@index')->name('home');
*/

