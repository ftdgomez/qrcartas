<?php

namespace App\Http\Controllers;
use \App\User;
use \App\Categoria;
use Illuminate\Http\Request;

class CartaController extends Controller
{
    public function setCatArr(User $user)
    {
        // si el usuario existe
        $catArr = [];
        $i = 0;
        foreach($user->categorias as $uc)
        {
            $platos = [];
            foreach ($uc->platos as $plato)
            {
                array_push($platos, [
                        "id" => $plato->id,
                        "cat_index" => $i,
                        "categoria_id" => $uc->id,
                        "title" => $plato->title,
                        "price" => $plato->price,
                        "description" => $plato->description,
                        "picture" => $plato->picture,
                        "alergenos" => $plato->alergenos
                ]);
            }
            array_push($catArr, [
                "id" => $uc->id,
                "name" => $uc->name,
                "platos" => $platos
            ]);
            $i++;
        }
        return $catArr;
    }
    public function index(string $username)
    {
        $user = User::where('username', $username)->first();
        if (!$user)
        {
            return response()->json([
                "msg" => sprintf('El usuario "%s" no existe.', $username)
            ], 400);
        }
        $catArr = $this->setCatArr($user);
        return view('carta', [
            "data" => [
                "username" => $user->username,
                "logo" => $user->logo,
                "color_primary" => $user->color_primary,
                "settings" => $user->settings,
                "carta_original" => $user->carta_original,
                "categorias" => $catArr
            ]
        ]);
    }
    public function Admin()
    {
        $user = auth()->user();
        $catArr = $this->setCatArr($user);
        return view('app', [
            "data" => [
                "username" => $user->username,
                "logo" => $user->logo,
                "color_primary" => $user->color_primary,
                "settings" => $user->settings,
                "carta_original" => $user->carta_original,
                "categorias" => $catArr
            ]
        ]);
    }
     public function show(string $username)
    {
        $user = User::where('username', $username)->first();
        if (!$user)
        {
            return response()->json([
                "msg" => sprintf('El usuario "%s" no existe.', $username)
            ], 400);
        }
        // si el usuario existe
        $catArr = $this->setCatArr($user);
        return response()->json([
            "username" => $user->username,
            "logo" => $user->logo,
            "color_primary" => $user->color_primary,
            "settings" => $user->settings,
            "carta_original" => $user->carta_original,
            "categorias" => $catArr
        ]);
    }
}
